package org.debz.model;

/**
 * User: manu
 * Date: 16/02/15
 * Time: 22:45
 */
public class SmsIn {
   private String sender,receiver,msg,senttime,receivedtime,status;
   private long id;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSenttime() {
        return senttime;
    }

    public void setSenttime(String senttime) {
        this.senttime = senttime;
    }

    public String getReceivedtime() {
        return receivedtime;
    }

    public void setReceivedtime(String receivedtime) {
        this.receivedtime = receivedtime;
    }

}
