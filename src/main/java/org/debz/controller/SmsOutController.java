package org.debz.controller;

import org.debz.model.SmsOut;
import org.debz.service.MemberGroupMapService;
import org.debz.service.SmsOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * User: manu
 * Date: 8/29/14
 * Time: 8:03 PM
 */
@Controller
public class SmsOutController {
    @Autowired
    private SmsOutService smsOutService;

    @RequestMapping(value = "/smsOutSave.json", method = RequestMethod.POST)
    @ResponseBody
    String updateSave(final @RequestBody Map<String, Object> map) {
        String receiver =  (String) map.get("phone_number");
        String msg =  (String) map.get("msg");

        SmsOut smsOut =new SmsOut();
        smsOut.setReceiver(receiver);
        smsOut.setMsg(msg);
        smsOut.setStatus("send");
        return smsOutService.saveSms(smsOut);
    }

    @Autowired
    private MemberGroupMapService memberGroupMapService;
    @RequestMapping(value = "/groupSmsOutSave.json", method = RequestMethod.POST)
    @ResponseBody
    String updateSaveGroup(final @RequestBody Map<String, Object> map) {
        String result ="0";
        long group_id = Long.valueOf((String) map.get("group_id"));
        String msg =  (String) map.get("msg");
        List<String> phoneNumberList = memberGroupMapService.getPhoneNumbersByGroupId(group_id);
        for(String phoneNumber :phoneNumberList){
            SmsOut smsOut =new SmsOut();
            smsOut.setReceiver(phoneNumber);
            smsOut.setMsg(msg);
            smsOut.setStatus("send");
            result = smsOutService.saveSms(smsOut);
        }
        return result;
    }

}
