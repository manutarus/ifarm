package org.debz.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debz.model.SmsIn;
import org.debz.service.SmsInService;
import org.debz.utils.WebConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: manu
 * Date: 8/29/14
 * Time: 8:03 PM
 */
@Controller
public class SmsInController {
    @Autowired
    private SmsInService smsInService;
    private final Log log = LogFactory.getLog(this.getClass());

    @RequestMapping(value = "/list/allSmsIns.json", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, Object> view(final @RequestParam(value = "search") String search,
                             final @RequestParam(value = "pageNumber") Integer pageNumber,
                             final @RequestParam(value = "pageSize") Integer pageSize) {
        Map<String, Object> response = new HashMap<String, Object>();

        List<Object> objects = new ArrayList<Object>();
        List<SmsIn> smsIns = smsInService.getSmsIns(search, pageNumber, pageSize);
        int pages = (smsInService.countSmsIns(search).intValue() + pageSize - 1) / pageSize;
        for (SmsIn SmsInList : smsIns) {
            objects.add(WebConverter.convertSmsIn(SmsInList));
        }
        response.put("pages", pages);
        response.put("objects", objects);
        return response;
    }

    @RequestMapping(value = "/singleSms.json",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getSmsIn(final @RequestParam(value = "id") String id) {
        return WebConverter.convertSmsIn(smsInService.getSmsInById(Long.valueOf(id)));
    }

}
