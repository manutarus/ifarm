package org.debz.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debz.model.Group;
import org.debz.service.GroupService;
import org.debz.utils.WebConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * User: manu
 * Date: 8/29/14
 * Time: 8:03 PM
 */
@Controller
public class GroupController {
    @Autowired
    private GroupService groupService;

    private final Log log = LogFactory.getLog(this.getClass());
    @RequestMapping(value = "/groupUpdateSave.json", method = RequestMethod.POST)
    @ResponseBody
    String updateSave(final @RequestBody Map<String, Object> map) {
        String uuid = (String) map.get("uuid");
        String name = (String) map.get("name");
        String describtion =  (String) map.get("describtion");
        String created_by =  (String) map.get("created_by");
        log.info("Create/Update group "+name);

        if (StringUtils.isNotBlank(uuid)) {
            Group group = groupService.getGroupByUuid(uuid);
            if (StringUtils.isNotBlank(name)||StringUtils.isNotBlank(describtion)) {
                group.setUuid(uuid);
                group.setName(name);
                group.setDescribtion(describtion);
                group.setCreated_by("admin");
                return groupService.saveGroup(group);
            }
            else {
                log.error("Missing all required fields");
                return "0";
            }
        }
        else {
            Group group = new Group();
            group.setName(name);
            group.setDescribtion(describtion);
            group.setCreated_by("admin");
            group.setUuid(UUID.randomUUID().toString());
            return groupService.saveGroup(group);
        }
    }

    @RequestMapping(value = "/list/allGroups.json", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, Object> view(final @RequestParam(value = "search") String search,
                             final @RequestParam(value = "pageNumber") Integer pageNumber,
                             final @RequestParam(value = "pageSize") Integer pageSize) {
        Map<String, Object> response = new HashMap<String, Object>();
        WebConverter webConverter = new WebConverter();
        List<Object> objects = new ArrayList<Object>();
        List<Group> groups = groupService.getGroups(search, pageNumber, pageSize);
        int pages = (groupService.countGroups(search).intValue() + pageSize - 1) / pageSize;
        for (Group groupList : groups) {
            objects.add(webConverter.convertGroup(groupList));
        }
        response.put("pages", pages);
        response.put("objects", objects);
        return response;
    }

    @RequestMapping(value = "/group.json",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getGroup(final @RequestParam(value = "uuid") String uuid) {
        WebConverter webConverter = new WebConverter();
        return webConverter.convertGroupMap(groupService.getGroupByUuid(uuid));
    }
}
