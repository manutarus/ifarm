package org.debz.controller;

import org.debz.model.MemberGroupMap;
import org.debz.service.MemberGroupMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * User: manu
 * Date: 8/29/14
 * Time: 8:03 PM
 */
@Controller
public class MemberGroupMapController {
    @Autowired
    private MemberGroupMapService memberGroupMapService;
    @RequestMapping(value = "/addMembers.json", method = RequestMethod.POST)
    @ResponseBody
    void updateSave(final @RequestBody Map<String, Object> map) {
        List<String> member_idList = (List<String>) map.get("member_idList");
        Long group_id = Long.valueOf((String) map.get("group_id"));
        for (String member_id : member_idList) {
            MemberGroupMap memberGroupMap = new MemberGroupMap();
            memberGroupMap.setMember_id(Long.valueOf(member_id));
            memberGroupMap.setGroup_id(group_id);
            memberGroupMap.setCreated_by("admin");
            memberGroupMap.setUuid(UUID.randomUUID().toString());
            memberGroupMapService.saveMemberGroupMap(memberGroupMap);
        }
    }
    @RequestMapping(value = "/dropMembers.json", method = RequestMethod.POST)
    @ResponseBody
    void dropMember(final @RequestBody Map<String, Object> map) {
        List<String> member_idList = (List<String>) map.get("member_idList");
        Long group_id = Long.valueOf((String) map.get("group_id"));
        for (String member_id : member_idList) {
            memberGroupMapService.dropMember(group_id,Long.valueOf(member_id));
        }
    }
}
