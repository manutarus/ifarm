package org.debz.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debz.model.Member;
import org.debz.service.MemberService;
import org.debz.utils.WebConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * User: manu
 * Date: 8/29/14
 * Time: 8:03 PM
 */
@Controller
public class MemberController {
    @Autowired
    private MemberService memberService;
    private final Log log = LogFactory.getLog(this.getClass());
    @RequestMapping(value = "/memberUpdateSave.json", method = RequestMethod.POST)
    @ResponseBody
    String updateSave(final @RequestBody Map<String, Object> map) {
        String uuid = (String) map.get("uuid");
        String sur_name = (String) map.get("sur_name");
        String first_name =  (String) map.get("first_name");
        String phone_number =  (String) map.get("phone_number");
        String location =  (String) map.get("location");
        log.info("Create/Update member "+location);

        if (StringUtils.isNotBlank(uuid)) {
            Member member = memberService.getMemberByUuid(uuid);
            if (StringUtils.isNotBlank(sur_name)||StringUtils.isNotBlank(first_name)
                    ||StringUtils.isNotBlank(phone_number)||StringUtils.isNotBlank(location)) {
                member.setUuid(uuid);
                member.setSur_name(sur_name);
                member.setFirst_name(first_name);
                member.setPhone_number(phone_number);
                member.setLocation(location);
                member.setCreated_by("admin");
                return memberService.saveMember(member);
            }
            else {
                log.error("Missing all required fields");
                return "0";
            }
        }
        else {
            Member member = new Member();
            member.setSur_name(sur_name);
            member.setFirst_name(first_name);
            member.setPhone_number(phone_number);
            member.setLocation(location);
            member.setCreated_by("admin");
            member.setUuid(UUID.randomUUID().toString());
            return memberService.saveMember(member);
        }
    }

    @RequestMapping(value = "/list/allMembers.json", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, Object> view(final @RequestParam(value = "search") String search,
                             final @RequestParam(value = "pageNumber") Integer pageNumber,
                             final @RequestParam(value = "pageSize") Integer pageSize) {
        Map<String, Object> response = new HashMap<String, Object>();

        List<Object> objects = new ArrayList<Object>();
        List<Member> members = memberService.getMembers(search, pageNumber, pageSize);
        int pages = (memberService.countMembers(search).intValue() + pageSize - 1) / pageSize;
        for (Member memberList : members) {
            objects.add(WebConverter.convertMember(memberList));
        }
        response.put("pages", pages);
        response.put("objects", objects);
        return response;
    }

    @RequestMapping(value = "/member.json",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getMember(final @RequestParam(value = "uuid") String uuid) {
        return WebConverter.convertMember(memberService.getMemberByUuid(uuid));
    }

    @RequestMapping(value = "/list/allMembersToDrop.json", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<String, Object> memberDrop(final @RequestParam(value = "group_id") String group_id,
                                   final @RequestParam(value = "search") String search,
                                   final @RequestParam(value = "pageNumber") Integer pageNumber,
                                   final @RequestParam(value = "pageSize") Integer pageSize) {
        Map<String, Object> response = new HashMap<String, Object>();

        List<Object> objects = new ArrayList<Object>();
        List<Member> members = memberService.getMembersToDrop(Long.valueOf(group_id), search, pageNumber, pageSize);
        int pages = (memberService.countMembersToDrop(Long.valueOf(group_id), search).intValue() + pageSize - 1) / pageSize;
        for (Member memberList : members) {
            objects.add(WebConverter.convertMember(memberList));
        }
        response.put("pages", pages);
        response.put("objects", objects);
        return response;
    }
}
