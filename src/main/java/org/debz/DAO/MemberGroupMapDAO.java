package org.debz.DAO;

import org.debz.model.MemberGroupMap;

import java.util.List;

/**
 * User: manu
 * Date: 12/02/15
 * Time: 12:32
 */
public interface MemberGroupMapDAO {
    public String saveMemberGroupMap(MemberGroupMap memberGroupMap);
    public List<String> getPhoneNumbersByGroupId(long group_id);
    public List countMemberGroups(Long group_id);
    public void dropMember(long group_id,Long member_id);
}
