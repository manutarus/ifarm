package org.debz.DAO.hibernate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debz.DAO.MemberDAO;
import org.debz.model.Member;
import org.debz.utils.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Member: tarus
 * Date: 9/16/13
 * Time: 12:30 PM
 */

@Repository
public class MemberDAOImpl implements MemberDAO {
    @Autowired
    SessionFactory sessionFactory;
    private Log log = LogFactory.getLog(this.getClass());
    SessionFactory sf = HibernateUtil.getSessionFactory();
    Session session = sf.openSession();
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public String saveMember(Member member) {
        try{
            sessionFactory.getCurrentSession().saveOrUpdate(member);
            log.info("Person saved successfully");
            return "1";
        }catch (Exception e){
            log.info("Person save Failed");
            return "0";
        }
    }
    public Member getMember(long id) {
        Member member = (Member) getCurrentSession().get(Member.class, id);
        return member;
    }

    public void updateMember(Member member) {
        Member memberToUpdate = getMember(member.getMember_id());
        getCurrentSession().update(memberToUpdate);
    }

    public void deleteMember(int id) {
        Member member = getMember(id);
        if (member != null)
            getCurrentSession().delete(member);
    }

    @SuppressWarnings("unchecked")
      @Transactional
    public List<Member> getMembers() {
        return sessionFactory.getCurrentSession().createQuery("from Member").list();
    }

    public List<Member> getMembers(final String search, final Integer pageNumber, final Integer pageSize) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Member.class);
        if (StringUtils.isNotEmpty(search)) {
            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.ilike("first_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("sur_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("phone_number", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("location", search, MatchMode.ANYWHERE));
            criteria.add(disjunction);
        }
        if (pageNumber != null) {
            criteria.setFirstResult((pageNumber - 1) * pageSize);
        }
        if (pageSize != null) {
            criteria.setMaxResults(pageSize);
        }
        criteria.addOrder(Order.desc("member_id"));
        return criteria.list();
    }
    public List<Member> getMembersToDrop(final long group_id, final String search, final Integer pageNumber, final Integer pageSize) {
        String group_hold= String.valueOf(group_id);
        Query q;
        if (StringUtils.isNotEmpty(search)) {
            q = session.createQuery("from Member where first_name like '%"+search+"%' or sur_name like '%"+search+"%' or phone_number like '%"+search+"%' or location like '%"+search+"%' and member_id in (select member_id from MemberGroupMap where group_id="+group_hold+")");
        } else {
            q  = session.createQuery("from Member where member_id in (select member_id from MemberGroupMap where group_id="+group_hold+")");
        }

        if (pageNumber != null) {
            q.setFirstResult((pageNumber - 1) * pageSize);
        }
        if (pageSize != null) {
            q.setMaxResults(pageSize);
        }
        List<Member> list= q.list();
//        session.close();

        return list;

    }
    public Number countMembersToDrop(Long group_id, String search){
        String group_hold= String.valueOf(group_id);
        Query q;
        if (StringUtils.isNotEmpty(search)) {
            q = session.createQuery("from Member where first_name like '%"+search+"%' or sur_name like '%"+search+"%' or phone_number like '%"+search+"%' or location like '%"+search+"%' and member_id in (select member_id from MemberGroupMap where group_id="+group_hold+")");
        } else {
            q  = session.createQuery("from Member where member_id in (select member_id from MemberGroupMap where group_id=" + group_hold + ")");
        }
        List<Member> list= q.list();
//        session.close();
        return list.size();
    }

    @Override
    public Number countMembers(String search) {
        Criteria criteria = getCurrentSession().createCriteria(Member.class);
        if (StringUtils.isNotEmpty(search)) {
            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.ilike("first_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("sur_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("phone_number", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("location", search, MatchMode.ANYWHERE));
            criteria.add(disjunction);
        }
        criteria.setProjection(Projections.rowCount());
        return (Number) criteria.uniqueResult();
    }

    public Member getMembersByName(String memberName){
        Criteria criteria = getCurrentSession().createCriteria(Member.class);
        criteria.add(Restrictions.eq("sur_name", memberName));
        return (Member) criteria.uniqueResult();
    }
    public Member getAgentByUuid(final String uuid){
        Criteria criteria = getCurrentSession().createCriteria(Member.class);
        criteria.add(Restrictions.eq("uuid", uuid));
        return (Member) criteria.uniqueResult();
    }

}
