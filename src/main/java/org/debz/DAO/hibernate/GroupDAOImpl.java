package org.debz.DAO.hibernate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debz.DAO.GroupDAO;
import org.debz.model.Group;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Group: tarus
 * Date: 9/16/13
 * Time: 12:30 PM
 */

@Repository
public class GroupDAOImpl implements GroupDAO {
    @Autowired
    SessionFactory sessionFactory;
    private Log log = LogFactory.getLog(this.getClass());

    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public String saveGroup(Group group) {
        try{
            sessionFactory.getCurrentSession().saveOrUpdate(group);
            log.info("Group saved successfully");
            return "1";
        }catch (Exception e){
            log.info("Group save Failed");
            return "0";
        }
    }

    public void updateGroup(Group group) {
        Group groupToUpdate = getGroup(group.getGroup_id());
        getCurrentSession().update(groupToUpdate);

    }

    public Group getGroup(long id) {
        Group group = (Group) getCurrentSession().get(Group.class, id);
        return group;
    }

    public void deleteGroup(int id) {
        Group group = getGroup(id);
        if (group != null)
            getCurrentSession().delete(group);
    }

    @SuppressWarnings("unchecked")
      @Transactional
    public List<Group> getGroups() {
        return sessionFactory.getCurrentSession().createQuery("from Group").list();
    }

    public List<Group> getGroups(final String search, final Integer pageNumber, final Integer pageSize) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Group.class);
        if (StringUtils.isNotEmpty(search)) {
            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.ilike("first_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("sur_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("phone_number", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("location", search, MatchMode.ANYWHERE));
            criteria.add(disjunction);
        }
        if (pageNumber != null) {
            criteria.setFirstResult((pageNumber - 1) * pageSize);
        }
        if (pageSize != null) {
            criteria.setMaxResults(pageSize);
        }
        criteria.addOrder(Order.desc("group_id"));
        return criteria.list();
    }

    @Override
    public Number countGroups(String search) {
        Criteria criteria = getCurrentSession().createCriteria(Group.class);
        if (StringUtils.isNotEmpty(search)) {
            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.ilike("first_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("sur_name", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("phone_number", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("location", search, MatchMode.ANYWHERE));
            criteria.add(disjunction);
        }
        criteria.setProjection(Projections.rowCount());
        return (Number) criteria.uniqueResult();
    }

    public Group getGroupsByName(String groupName){
        Criteria criteria = getCurrentSession().createCriteria(Group.class);
        criteria.add(Restrictions.eq("sur_name", groupName));
        return (Group) criteria.uniqueResult();
    }
    public Group getAgentByUuid(final String uuid){
        Criteria criteria = getCurrentSession().createCriteria(Group.class);
        criteria.add(Restrictions.eq("uuid", uuid));
        return (Group) criteria.uniqueResult();
    }

}
