package org.debz.DAO.hibernate;

import org.debz.DAO.SmsOutDAO;
import org.debz.model.SmsOut;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Member: tarus
 * Date: 9/16/13
 * Time: 12:30 PM
 */

@Repository
public class SmsOutDAOImpl implements SmsOutDAO {
    @Autowired
    SessionFactory sessionFactory;
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public String saveSms(SmsOut smsOut) {
        try{
            getCurrentSession().saveOrUpdate(smsOut);
            return "1";
        }catch (Exception e){
            return "0";
        }
    }
}
