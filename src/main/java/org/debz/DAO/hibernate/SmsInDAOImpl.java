package org.debz.DAO.hibernate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debz.DAO.SmsInDAO;
import org.debz.model.SmsIn;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * SmsIn: tarus
 * Date: 9/16/13
 * Time: 12:30 PM
 */

@Repository
public class SmsInDAOImpl implements SmsInDAO {
    @Autowired
    SessionFactory sessionFactory;
    private Log log = LogFactory.getLog(this.getClass());
    
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public void updateSms(SmsIn smsIn) {
        getCurrentSession().update(smsIn);
    }
    @Transactional
    @Override
    public SmsIn getSmsInById(long id) {
        SmsIn smsIn = (SmsIn) getCurrentSession().get(SmsIn.class, id);
        return smsIn;
    }
    @Transactional
    @Override
    public List<SmsIn> getSmsIns(final String search, final Integer pageNumber, final Integer pageSize) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(SmsIn.class);
        if (StringUtils.isNotEmpty(search)) {
            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.ilike("sender", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("receiver", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("msg", search, MatchMode.ANYWHERE));
            criteria.add(disjunction);
        }
        if (pageNumber != null) {
            criteria.setFirstResult((pageNumber - 1) * pageSize);
        }
        if (pageSize != null) {
            criteria.setMaxResults(pageSize);
        }
        criteria.addOrder(Order.desc("id"));
        return criteria.list();
    }

    @Transactional
    @Override
    public Number countSmsIns(String search) {
        Criteria criteria = getCurrentSession().createCriteria(SmsIn.class);
        if (StringUtils.isNotEmpty(search)) {
            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.ilike("sender", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("receiver", search, MatchMode.ANYWHERE));
            disjunction.add(Restrictions.ilike("msg", search, MatchMode.ANYWHERE));
            criteria.add(disjunction);
        }
        criteria.setProjection(Projections.rowCount());
        return (Number) criteria.uniqueResult();
    }



}
