package org.debz.DAO.hibernate;

import org.debz.DAO.MemberGroupMapDAO;
import org.debz.model.MemberGroupMap;
import org.debz.service.MemberService;
import org.debz.utils.HibernateUtil;
import org.hibernate.*;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Member: tarus
 * Date: 9/16/13
 * Time: 12:30 PM
 */

@Repository
public class MemberGroupMapDAOImpl implements MemberGroupMapDAO {
    @Autowired
    SessionFactory sessionFactory;
    @Autowired
    private MemberService memberService;
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional
    @Override
    public String saveMemberGroupMap(MemberGroupMap memberGroupMap){
        try{
            getCurrentSession().saveOrUpdate(memberGroupMap);
            return "1";
        }catch (Exception e){
            return "0";
        }
    }

    @Transactional
    @Override
    public List countMemberGroups(Long group_id) {
        String sql = "SELECT group_id FROM member_group_map";
        SQLQuery query = getCurrentSession().createSQLQuery(sql);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
        List results = query.list();
        return results;
    }
    @Transactional
    @Override
    public List<String> getPhoneNumbersByGroupId(long group_id){
        Criteria criteria = getCurrentSession().createCriteria(MemberGroupMap.class);
        if (group_id>0) {
            Disjunction disjunction = Restrictions.disjunction();
            disjunction.add(Restrictions.eq("group_id", group_id));
            criteria.add(disjunction);
        }
        List<MemberGroupMap> memberGroupMapList = criteria.list();
        List<String> phoneNumbers = new ArrayList<String>();
        for (MemberGroupMap memberGroupMap : memberGroupMapList) {
            phoneNumbers.add(memberService.getMember(memberGroupMap.getMember_id()).getPhone_number());
        }
        return phoneNumbers;
    }
    @Transactional
    @Override
    public void dropMember(long group_id,Long member_id) {

        if (group_id > 0 && member_id > 0 ) {
            SessionFactory sf = HibernateUtil.getSessionFactory();
            Session session = sf.openSession();
            Query q = session.createQuery("delete from MemberGroupMap where member_id ="+member_id+" and group_id ="+group_id);
            q.executeUpdate();
            session.close();
        }

    }

}
