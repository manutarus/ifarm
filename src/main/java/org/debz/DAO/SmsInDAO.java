package org.debz.DAO;

import org.debz.model.SmsIn;

import java.util.List;

/**
 * User: manu
 * Date: 12/02/15
 * Time: 12:32
 */
public interface SmsInDAO {
    public void updateSms(SmsIn smsIn);
    public List<SmsIn> getSmsIns(final String search, final Integer pageNumber, final Integer pageSize);
    public Number countSmsIns(String search);
    public SmsIn getSmsInById(long id);
}
