package org.debz.DAO;

import org.debz.model.Group;

import java.util.List;

/**
 * User: manu
 * Date: 12/02/15
 * Time: 12:32
 */
public interface GroupDAO {
    public String saveGroup(Group group);
    public void updateGroup(Group group);
    public Group getGroup(long id);
    public void deleteGroup(int id);
    public List<Group> getGroups();
    public List<Group> getGroups(final String search, final Integer pageNumber, final Integer pageSize);
    public Number countGroups(String search);
    public Group getGroupsByName(String name);
    public Group getAgentByUuid(final String uuid);
}
