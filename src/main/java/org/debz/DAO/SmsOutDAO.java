package org.debz.DAO;

import org.debz.model.SmsOut;

/**
 * User: manu
 * Date: 12/02/15
 * Time: 12:32
 */
public interface SmsOutDAO {
    public String saveSms(SmsOut smsOut);
}
