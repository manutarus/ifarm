package org.debz.DAO;

import org.debz.model.Member;

import java.util.List;

/**
 * User: manu
 * Date: 12/02/15
 * Time: 12:32
 */
public interface MemberDAO {
    public String saveMember(Member member);
    public void updateMember(Member member);
    public Member getMember(long id);
    public void deleteMember(int id);
    public List<Member> getMembers();
    public List<Member> getMembers(final String search, final Integer pageNumber, final Integer pageSize);
    public List<Member> getMembersToDrop(final long group_id, final String search, final Integer pageNumber, final Integer pageSize);
    public Number countMembers(String search);
    public Number countMembersToDrop(Long group_id, String search);
//    public Member getMembersByName(final long group_id, String surName);
    public Member getMembersByName(String username);
    public Member getAgentByUuid(final String uuid);
}
