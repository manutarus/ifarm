/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.debz.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debz.model.Group;
import org.debz.model.Member;
import org.debz.model.SmsIn;
import org.debz.model.User;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Write brief description about the class here.
 */
public class WebConverter {
    private final Log log = LogFactory.getLog(this.getClass());
    public static Map<String, Object> convertUser(final User user) {
        Map<String, Object> converted = new HashMap<String, Object>();
        if (user != null) {
            converted.put("f_name", user.getF_name());
            converted.put("o_name", user.getO_name());
            converted.put("level", user.getLevel());
        }
        return converted;
    }
    public static Map<String, Object> convertMember(final Member member) {
        Map<String, Object> converted = new HashMap<String, Object>();
        if (member != null) {
            converted.put("member_id", member.getMember_id());
            converted.put("sur_name", member.getSur_name());
            converted.put("first_name",member.getFirst_name());
            converted.put("phone_number",member.getPhone_number());
            converted.put("location",member.getLocation());
            converted.put("uuid",member.getUuid());
        }
        return converted;
    }
    public Map<String, Object> convertGroup(final Group group) {

        Map<String, Object> converted = new HashMap<String, Object>();
        if (group != null) {
            converted.put("group_id", group.getGroup_id());
            converted.put("name", group.getName());
            converted.put("describtion",group.getDescribtion());
            converted.put("date_created",group.getDate_created());
            converted.put("created_by",group.getCreated_by());
            converted.put("uuid",group.getUuid());
        }
        return converted;
    }
    public Map<String, Object> convertGroupMap(final Group group) {
        NativeSql nativeSql = new NativeSql();
        Map<String, Object> converted = new HashMap<String, Object>();
        if (group != null) {
            converted.put("group_id", group.getGroup_id());
            converted.put("name", group.getName());
            converted.put("describtion",group.getDescribtion());
            converted.put("date_created",group.getDate_created());
            converted.put("created_by",group.getCreated_by());
            converted.put("uuid",group.getUuid());
             log.info("groupCount "+nativeSql.checkMemberExist(group.getGroup_id()));
            if(nativeSql.checkMemberExist(group.getGroup_id())){
                converted.put("status","1");
            }

        }
        return converted;
    }

    public static Map<String, Object> convertSmsIn(final SmsIn smsIn) {
        Map<String, Object> converted = new HashMap<String, Object>();
        if (smsIn != null) {
            if(smsIn.getStatus().equals("1")){
                converted.put("id", smsIn.getId());
                converted.put("sender", smsIn.getSender());
                converted.put("msg",smsIn.getMsg());

            }
        }
        return converted;
    }

}