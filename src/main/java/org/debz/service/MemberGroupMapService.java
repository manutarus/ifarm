package org.debz.service;

import org.debz.model.MemberGroupMap;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: tarus
 * Date: 9/16/13
 * Time: 12:55 PM
 */

@Transactional
public interface MemberGroupMapService {
    public String saveMemberGroupMap(MemberGroupMap memberGroupMap);
    public List<String> getPhoneNumbersByGroupId(long group_id);
    public List countMemberGroups(Long group_id);
    public void dropMember(long group_id,Long member_id);
}
