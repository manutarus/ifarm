package org.debz.service;

import org.debz.model.Group;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: tarus
 * Date: 9/16/13
 * Time: 12:55 PM
 */

@Transactional
public interface GroupService {

    public String saveGroup(Group group);
    public void updateGroup(Group group);
    public Group getGroup(long id);
    public void deleteGroup(int id);
    public List<Group> getGroups();
    public List<Group> getGroups(final String search, final Integer pageNumber, final Integer pageSize);
    public Number countGroups(String search);
    public Group getGroupsByName(String name);
    public Group getGroupByUuid(final String uuid);

}
