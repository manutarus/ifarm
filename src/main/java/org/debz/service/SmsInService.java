package org.debz.service;

import org.debz.model.SmsIn;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: tarus
 * Date: 9/16/13
 * Time: 12:55 PM
 */

@Transactional
public interface SmsInService {
    public void updateSms(SmsIn smsIn);
    public List<SmsIn> getSmsIns(final String search, final Integer pageNumber, final Integer pageSize);
    public Number countSmsIns(String search);
    public SmsIn getSmsInById(long id);

}
