package org.debz.service;

import org.debz.model.Member;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: tarus
 * Date: 9/16/13
 * Time: 12:55 PM
 */

@Transactional
public interface MemberService {

    public String saveMember(Member member);
    public void updateMember(Member member);
    public Member getMember(long id);
    public void deleteMember(int id);
    public List<Member> getMembers();
    public List<Member> getMembers(final String search, final Integer pageNumber, final Integer pageSize);
    public List<Member> getMembersToDrop(final long group_id, final String search, final Integer pageNumber, final Integer pageSize);
    public Number countMembers(String search);
    public Number countMembersToDrop(Long group_id, String search);
    public Member getMembersByName(String surName);
    public Member getMemberByUuid(final String uuid);

}
