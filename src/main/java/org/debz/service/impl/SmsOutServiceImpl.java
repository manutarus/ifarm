package org.debz.service.impl;

import org.debz.DAO.SmsOutDAO;
import org.debz.model.SmsOut;
import org.debz.service.SmsOutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Member: tarus
 * Date: 9/16/13
 * Time: 12:59 PM
 */

@Transactional
public class SmsOutServiceImpl implements SmsOutService {
    
    @Autowired
    public SmsOutDAO smsOutDAO;

    public String saveSms(SmsOut smsOut) {
        return smsOutDAO.saveSms(smsOut);

    }

}
