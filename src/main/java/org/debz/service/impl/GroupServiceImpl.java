package org.debz.service.impl;

import org.debz.DAO.GroupDAO;
import org.debz.model.Group;
import org.debz.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Group: tarus
 * Date: 9/16/13
 * Time: 12:59 PM
 */

@Transactional
public class GroupServiceImpl implements GroupService {
    
    @Autowired
    public GroupDAO groupDAO;

    public String saveGroup(Group group) {
        return groupDAO.saveGroup(group);

    }

    public void updateGroup(Group group) {
        groupDAO.updateGroup(group);
    }

    public Group getGroup(long id) {
        return groupDAO.getGroup(id);
    }

    public void deleteGroup(int id) {
        groupDAO.deleteGroup(id);
    }

    public List<Group> getGroups() {
        return groupDAO.getGroups();
    }
    public List<Group> getGroups(final String search, final Integer pageNumber, final Integer pageSize) {
        return groupDAO.getGroups(search, pageNumber, pageSize);
    }

    public Number countGroups(String search) {
        return groupDAO.countGroups(search);
    }

    public Group getGroupsByName(String surName){
        return groupDAO.getGroupsByName(surName);
    }

    public Group getGroupByUuid(final String uuid){
        return groupDAO.getAgentByUuid(uuid);
    }

}
