package org.debz.service.impl;

import org.debz.DAO.SmsInDAO;
import org.debz.model.SmsIn;
import org.debz.service.SmsInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Member: tarus
 * Date: 9/16/13
 * Time: 12:59 PM
 */

@Transactional
public class SmsInServiceImpl implements SmsInService {
    
    @Autowired
    public SmsInDAO smsInDAO;

    public void updateSms(SmsIn smsIn){
        smsInDAO.updateSms(smsIn);
    }
    public List<SmsIn> getSmsIns(final String search, final Integer pageNumber, final Integer pageSize){
        return smsInDAO.getSmsIns(search,pageNumber,pageSize);
    }
    public Number countSmsIns(String search){
        return smsInDAO.countSmsIns(search);
    }
    public SmsIn getSmsInById(long id){
        return smsInDAO.getSmsInById(id);
    }

}
