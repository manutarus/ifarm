package org.debz.service.impl;

import org.debz.DAO.MemberGroupMapDAO;
import org.debz.model.MemberGroupMap;
import org.debz.service.MemberGroupMapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Member: tarus
 * Date: 9/16/13
 * Time: 12:59 PM
 */

@Transactional
public class MemberGroupServiceImpl implements MemberGroupMapService {

    @Autowired
    public MemberGroupMapDAO memberGroupMapDAO;

    public String saveMemberGroupMap(MemberGroupMap memberGroupMap) {
        return memberGroupMapDAO.saveMemberGroupMap(memberGroupMap);
    }
    public List<String> getPhoneNumbersByGroupId(long group_id) {
        return memberGroupMapDAO.getPhoneNumbersByGroupId(group_id);
    }
    public List countMemberGroups(Long group_id){
        return memberGroupMapDAO.countMemberGroups(group_id);
    }

    public void dropMember(long group_id,Long member_id){
        memberGroupMapDAO.dropMember(group_id,member_id);
    }
}
