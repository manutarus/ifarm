package org.debz.service.impl;

import org.debz.DAO.MemberDAO;
import org.debz.model.Member;
import org.debz.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Member: tarus
 * Date: 9/16/13
 * Time: 12:59 PM
 */

@Transactional
public class MemberServiceImpl implements MemberService {
    
    @Autowired
    public MemberDAO memberDAO;

    public String saveMember(Member member) {
        return memberDAO.saveMember(member);

    }

    public void updateMember(Member member) {
        memberDAO.updateMember(member);
    }

    public Member getMember(long id) {
        return memberDAO.getMember(id);
    }

    public void deleteMember(int id) {
        memberDAO.deleteMember(id);
    }

    public List<Member> getMembers() {
        return memberDAO.getMembers();
    }
    public List<Member> getMembers(final String search, final Integer pageNumber, final Integer pageSize) {
        return memberDAO.getMembers(search, pageNumber, pageSize);
    }
    public List<Member> getMembersToDrop(final long group_id,final String search, final Integer pageNumber, final Integer pageSize) {
        return memberDAO.getMembersToDrop(group_id, search, pageNumber, pageSize);
    }
    public Number countMembers(String search) {
        return memberDAO.countMembers(search);
    }
    public Number countMembersToDrop(Long group_id, String search) {
        return memberDAO.countMembersToDrop(group_id,search);
    }
    public Member getMembersByName(String surName){
        return memberDAO.getMembersByName(surName);
    }

    public Member getMemberByUuid(final String uuid){
        return memberDAO.getAgentByUuid(uuid);
    }

}
