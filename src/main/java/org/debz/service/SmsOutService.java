package org.debz.service;

import org.debz.model.SmsOut;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: tarus
 * Date: 9/16/13
 * Time: 12:55 PM
 */

@Transactional
public interface SmsOutService {

    public String saveSms(SmsOut smsOut);
}
