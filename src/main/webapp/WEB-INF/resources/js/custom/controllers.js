
ifarmMod.controller('HomeCtrl', function($scope) {

    $scope.message = 'Trust and Commitment';

});

ifarmMod.controller('MembersCtrl', function($scope, $location, $memberService,$routeParams, $groupService,Data) {
    // initialize selected error data for re-queueing
    $scope.selected = {};
    // initialize the paging structure
    $scope.maxSize = 5;
    $scope.pageSize = 5;
    $scope.currentPage = 1;
    $memberService.getMembers($scope.search, $scope.currentPage, $scope.pageSize).
        then(function (response) {
            var serverData = response.data;
            $scope.members = serverData.objects;
            $scope.noOfPages = serverData.pages;
        });
    $scope.$watch('currentPage', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $memberService.getMembers($scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.members = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);

    $scope.$watch('search', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.currentPage = 1;
            $memberService.getMembers($scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.members = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);
    $scope.addMembers = function () {
        var member_idList = [];
        var selected_checks = document.querySelectorAll('input[name="checkSlave[]"]:checked');
        for(var x = 0, l = selected_checks.length; x < l;  x++)
        {
            member_idList.push(selected_checks[x].value);
        }
        $groupService.addMembers(member_idList, Data.getGroupID()).
            then(function () {
                $location.path("/groups");
            })
    };

} );
ifarmMod.controller('SmsInCtrl', function($scope, $location, $smsInService,$routeParams, $groupService,Data) {
    // initialize selected error data for re-queueing
    $scope.selected = {};
    // initialize the paging structure
    $scope.maxSize = 5;
    $scope.pageSize = 5;
    $scope.currentPage = 1;
    $smsInService.getSmsIns($scope.search, $scope.currentPage, $scope.pageSize).
        then(function (response) {
            var serverData = response.data;
            $scope.smsIns = serverData.objects;
            $scope.noOfPages = serverData.pages;
        });
    $scope.$watch('currentPage', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $smsInService.getSmsIns($scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.smsIns = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);

    $scope.$watch('search', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.currentPage = 1;
            $smsInService.getSmsIns($scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.smsIns = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);

} );

ifarmMod.controller('MembersDopCtrl', function($scope, $location, $memberService,$routeParams, $groupService,Data,$filter) {
    // initialize selected error data for re-queueing
    $scope.selected = {};
    $scope.master = {};
    // initialize the paging structure
    $scope.maxSize = 5;
    $scope.pageSize = 5;
    $scope.currentPage = 1;
    $memberService.getDropMembers(Data.getGroupID(),$scope.search, $scope.currentPage, $scope.pageSize).
        then(function (response) {
            var serverData = response.data;
            $scope.members = serverData.objects;
            $scope.noOfPages = serverData.pages;
        });

    $scope.$watch('currentPage', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $memberService.getDropMembers(Data.getGroupID(),$scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.members = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);

    $scope.$watch('search', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.currentPage = 1;
            $memberService.getDropMembers(Data.getGroupID(),$scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.members = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);

    $scope.dropMembers = function () {
        var member_idList = [];
        var selected_checks = document.querySelectorAll('input[name="checkSlave[]"]:checked');
        for(var x = 0, l = selected_checks.length; x < l;  x++)
        {
            member_idList.push(selected_checks[x].value);
        }
        $groupService.dropMembers(member_idList, Data.getGroupID()).
            then(function () {
                $location.path("/groups");
            })
    };

} );
ifarmMod.controller('GroupsCtrl', function($scope, $location, $groupService) {
    // initialize selected error data for re-queueing
    $scope.selected = {};
    // initialize the paging structure
    $scope.maxSize = 5;
    $scope.pageSize = 5;
    $scope.currentPage = 1;
    $groupService.getGroups($scope.search, $scope.currentPage, $scope.pageSize).
        then(function (response) {
            var serverData = response.data;
            $scope.groups = serverData.objects;
            $scope.noOfPages = serverData.pages;
        });

    $scope.$watch('currentPage', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $groupService.getGroups($scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.groups = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);

    $scope.$watch('search', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.currentPage = 1;
            $groupService.getGroups($scope.search, $scope.currentPage, $scope.pageSize).
                then(function (response) {
                    var serverData = response.data;
                    $scope.groups = serverData.objects;
                    $scope.noOfPages = serverData.pages;
                });
        }
    }, true);

} );

ifarmMod.controller('MemberCtrl', function($scope, $routeParams, $location, $memberService) {
    // initialize the source object
    $scope.source = {};
    // initialize the view to be read only
    $scope.mode = "view";
    $scope.uuid = $routeParams.uuid;
    if ($scope.uuid === undefined) {
        $scope.mode = "edit";
    } else {
        $memberService.getMember($scope.uuid).
            then(function(response) {
                $scope.member = response.data;
            });
    }

    $scope.edit = function() {
        $scope.mode = "edit";
    };

    $scope.textMode = function() {
        $scope.mode = "text";
    };

    $scope.cancel = function() {
        $location.path("/members");
    };

    $scope.save = function(member) {
        if ( member.first_name === undefined || member.sur_name === undefined || member.phone_number === undefined
            || member.location === undefined ) {
        //The member wont be saved
        }else{
            $memberService.saveMember(member.first_name,member.sur_name,member.phone_number,member.location,member.uuid).

                success(function(){
                    $location.path("/members");
                })
                .error(function(){
                    $location.path("/members");
                })
        }

    };

    $scope.text = function(phone_number,msg) {
        if ( msg === undefined) {

        }else{
            $memberService.singleText(phone_number,msg).
                success(function(){
                    $location.path("/members");
                })
                .error(function(){
                    $location.path("/members");
                })
        }
    };

});
ifarmMod.controller('GroupCtrl', function($scope, $routeParams, $location, $groupService,Data) {
    // initialize the source object
    $scope.source = {};
    // initialize the view to be read only
    $scope.mode = "view";
    $scope.uuid = $routeParams.uuid;
    $scope.status = $routeParams.status;
    if ($scope.uuid === undefined) {
        $scope.mode = "edit";
    } else {
        $groupService.getGroup($scope.uuid).
            then(function(response) {
                $scope.group = response.data;
            });
    }

    $scope.edit = function() {
        $scope.mode = "edit";
    };

    $scope.textMode = function() {
        if($scope.group.status === undefined ){
            $scope.mode = "noTextView";
        } else{
            $scope.mode = "text";
        }
    };

    $scope.memberAdd = function(group_id) {
        Data.setGroupID(group_id);
        $location.path("/addMembers");
    };
    $scope.memberDrop = function(group_id) {
        Data.setGroupID(group_id);
        $location.path("/dropMembers");
    };

    $scope.cancel = function() {
        $location.path("/groups");
    };

    $scope.save = function(group) {
        if ( group.name === undefined || group.describtion === undefined) {
        //The group wont be saved
        }else{
            $groupService.saveGroup(group.name,group.describtion,group.uuid).

                success(function(){
                    $location.path("/groups");
                })
                .error(function(){
                    $location.path("/groups");
                })
        }

    };

    $scope.sendGroupText = function(group_id,msg) {
        if ( msg === undefined) {

        }else{
            $groupService.groupText(group_id,msg).
                success(function(){
                    $location.path("/groups");
                })
                .error(function(){
                    $location.path("/groups");
                })
        }
    };


});
ifarmMod.controller('SmsCtrl', function($scope, $routeParams, $location, $smsInService,$memberService) {
    // initialize the source object
    $scope.source = {};
    // initialize the view to be read only
    $scope.mode = "view";
    $scope.id = $routeParams.id;
    if ($scope.id === undefined) {
        $scope.mode = "edit";
    } else {
        $smsInService.getSms($scope.id).
            then(function(response) {
                $scope.smsIn = response.data;
            });
    }


    $scope.cancel = function() {
        $location.path("/smsIns");
    };
    $scope.reply = function() {
        $scope.mode = "TextView";
    };
    $scope.text = function(phone_number,msg) {
        if ( msg === undefined) {

        }else{
            $memberService.singleText(phone_number,msg).
                success(function(){
                    $location.path("/smsIns");
                })
                .error(function(){
                    $location.path("/smsIns");
                })
        }
    };

});



