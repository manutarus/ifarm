var ifarmMod = angular.module('ifarmMod', ['ui.bootstrap'])

ifarmMod.
    config(['$routeProvider', '$compileProvider', function ($routeProvider, $compileProvider) {
        $compileProvider.urlSanitizationWhitelist(/^\s*(https?|ftp|mailto|file):/);

        $routeProvider.when('/view', {controller: 'HomeCtrl',
            templateUrl: 'resources/partials/welcome.html'});
        $routeProvider.when('/members', {controller: 'MembersCtrl',
            templateUrl: 'resources/partials/members.html'});
        $routeProvider.when('/member/:uuid', {controller: 'MemberCtrl',
            templateUrl: 'resources/partials/member.html'});
        $routeProvider.when('/createMember/', {controller: 'MemberCtrl',
            templateUrl: 'resources/partials/member.html'});

        $routeProvider.when('/groups', {controller: 'GroupsCtrl',
            templateUrl: 'resources/partials/groups.html'});
        $routeProvider.when('/group/:uuid', {controller: 'GroupCtrl',
            templateUrl: 'resources/partials/group.html'});
        $routeProvider.when('/createGroup/', {controller: 'GroupCtrl',
            templateUrl: 'resources/partials/group.html'});

        $routeProvider.when('/addMembers/', {controller: 'MembersCtrl',
            templateUrl: 'resources/partials/addMembers.html'});
        $routeProvider.when('/dropMembers/', {controller: 'MembersDopCtrl',
            templateUrl: 'resources/partials/dropMembers.html'});

        $routeProvider.when('/smsIns/', {controller: 'SmsInCtrl',
            templateUrl: 'resources/partials/smsIns.html'});
        $routeProvider.when('/smsIn/:id', {controller: 'SmsCtrl',
            templateUrl: 'resources/partials/smsIn.html'});

        $routeProvider.otherwise({redirectTo: '/view'});
    }]);


ifarmMod.factory('$memberService', function ($http) {
    var getMembers = function (search, pageNumber, pageSize) {
        if (search === undefined) {
            // replace undefined search term with empty string
            search = '';
        }
        return $http.get("list/allMembers.json?search=" + search + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize);
    };
    var getMember = function (uuid) {
        return $http.get("member.json?uuid=" + uuid);
    };
    var saveMember = function (first_name,sur_name,phone_number,location,uuid) {
        return $http.post("memberUpdateSave.json", {"first_name": first_name,"sur_name":sur_name, "phone_number": phone_number, "location":location, "uuid": uuid});
    };
    var singleText = function (phone_number,msg) {
        return $http.post("smsOutSave.json", {"phone_number": phone_number,"msg":msg});
    };
    var getDropMembers = function (group_id, search, pageNumber, pageSize) {
        if (search === undefined) {
            // replace undefined search term with empty string
            search = '';
        }
        return $http.get("list/allMembersToDrop.json?group_id=" + group_id + "&search=" + search + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize);
    };
    var getSmsIns = function (search, pageNumber, pageSize) {
        if (search === undefined) {
            // replace undefined search term with empty string
            search = '';
        }
        return $http.get("list/allSmsIns.json?search=" + search + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize);
    };
    return {
        getMembers: getMembers,
        getDropMembers: getDropMembers,
        getMember: getMember,
        saveMember: saveMember,
        singleText: singleText
    }
});

ifarmMod.factory('$smsInService', function ($http) {

    var getSmsIns = function (search, pageNumber, pageSize) {
        if (search === undefined) {
            // replace undefined search term with empty string
            search = '';
        }
        return $http.get("list/allSmsIns.json?search=" + search + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize);
    };
    var getSms = function (id) {
        return $http.get("singleSms.json?id=" + id);
    };
    return {
        getSmsIns: getSmsIns,
        getSms: getSms
    }
});
ifarmMod.factory('$groupService', function ($http) {
    var getGroups = function (search, pageNumber, pageSize) {
        if (search === undefined) {
            // replace undefined search term with empty string
            search = '';
        }
        return $http.get("list/allGroups.json?search=" + search + "&pageNumber=" + pageNumber + "&pageSize=" + pageSize);
    };
    var getGroup = function (uuid) {
        return $http.get("group.json?uuid=" + uuid);
    };
    var saveGroup = function (name,describtion,uuid) {
        return $http.post("groupUpdateSave.json", {"name": name,"describtion":describtion, "uuid": uuid});
    };
    var groupText = function (group_id,msg) {
        return $http.post("groupSmsOutSave.json", {"group_id": ""+group_id,"msg":msg});
    };
    var addMembers = function (member_idList,group_id) {
        return $http.post("addMembers.json", {"member_idList": member_idList,"group_id":""+group_id});
    };
    var dropMembers = function (member_idList,group_id) {
        return $http.post("dropMembers.json", {"member_idList": member_idList,"group_id":""+group_id});
    };

    return {
        getGroups: getGroups,
        getGroup: getGroup,
        saveGroup: saveGroup,
        groupText: groupText,
        addMembers: addMembers,
        dropMembers: dropMembers
    }
});

ifarmMod.factory('Data', function () {
    var data =
    {
        GroupID: ''
    };

    return {
        getGroupID: function () {
            return data.GroupID;
        },
        setGroupID: function (GroupID) {
            data.GroupID = GroupID;
        }
    };
});


