<!DOCTYPE HTML>
<html xmlns:ng="http://angularjs.org">
<head>
    <title>ifarm</title>
</head>
<body ng-app="ifarmMod">
<div id="header">
    <p><p></p></p>
    <div id="logo" class="fl_left">
        <img src="resources/ezk/images/ifarm_logo1_1.png"  alt="logo" style="width:400px;height:100px">
       <br> Keeping Uasin Gishu farmers County Informed
    </div>
</div>
<div id="sidebar">
    <ng:include src="'resources/partials/side.html'"></ng:include>
</div>
<div id="content">
    <ng:view></ng:view>
</div>
<script type="text/javascript">


</script>
<script src="resources/js/angular/angular.min.js"></script>
<script src="resources/js/angular/angular-animate.js"></script>
<script src="resources/js/angular/angular-resource.js"></script>
<script src="resources/js/custom/app.js"></script>
<script src="resources/js/custom/controllers.js"></script>
<link rel="stylesheet" href="resources/styles/custom/custom.css"/>
<link rel="stylesheet" href="resources/styles/custom/angular-material.css"/>
<link rel="stylesheet" href="resources/styles/bootstrap/css/bootstrap.css"/>
<link rel="stylesheet" href="resources/styles/bootstrap/css/pagination.css"/>
<script src="resources/js/angular/angular-resource.js"/>
<link data-require="bootstrap-css@2.3.2" data-semver="2.3.2" rel="stylesheet" href="resources/styles/bootstrap/css/bootstrap-combined.min.css" />
<script data-require="angular.js@1.1.5" data-semver="1.1.5" src="resources/js/jquery/jquery.js"></script>
<script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="resources/js/ui-bootstrap/ui-bootstrap-custom-tpls-0.4.0.js"></script>
</body>

</html>

