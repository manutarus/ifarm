<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>I-farm Farming Alert</title>
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!--
    -->
    <link href="resources/ezk/css/tooplate_style.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="resources/ezk/css/nivo-slider.css" type="text/css" media="screen" />

    <script language="javascript" type="text/javascript">
        function clearText(field)
        {
            if (field.defaultValue == field.value) field.value = '';
            else if (field.value == '') field.value = field.defaultValue;
        }
    </script>

    <link rel="stylesheet" type="text/css" href="resources/ezk/css/ddsmoothmenu.css" />

    <script type="text/javascript" src="resources/ezk/js/jquery.min.js"></script>
    <script type="text/javascript" src="resources/ezk/js/ddsmoothmenu.js">

    </script>

    <script type="text/javascript">

        ddsmoothmenu.init({
            mainmenuid: "tooplate_menu", //menu DIV id
            orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
            classname: 'ddsmoothmenu', //class added to menu's outer DIV
            //customtheme: ["#1c5a80", "#18374a"],
            contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
        })

    </script>

</head>
<body>


<div id="tooplate_header">
    <div id="tooplate_top">
        <img src="resources/ezk/images/ifarm_logo1_1.png" alt="image" />

        <div id="tooplate_search">

            <form id="login" action='view' method="POST" autocomplete="off">
                <input style="width: 150px" type="text" name="username"  placeholder="username"  required>

                <input style="width: 150px"  type="password"  name="password" placeholder="password" required>
                <br><button>Sign in</button>   <a href="" class="forgot_link">&nbsp;&nbsp;&nbsp;&nbsp;forgot password ?</a>
            </form>

            <div class="col_3">


                <div class="cleaner h10"></div>
                <p> </p>
                <div id="tooplate_titlebar">
                    <h5><marquee> <label style="color:greenyellow"> Get Informed and Increase your productivity</label> </marquee></h5>
                </div>
            </div>
        </div>
    </div>
    <div id="tooplate_mid_wrapper">
        <div id="tooplate_mid_home">

            <div id="slider-wrapper">

                <div id="slider" class="nivoSlider">
                    <img src="resources/ezk/images/slider/01.jpg" alt="Image 01" title="Cabagges planted on the swampy areas of UG county" />
                    <img src="resources/ezk/images/slider/02.jpg" alt="Image 02" title="Fresian dairy cows grazing on green fields" />
                    <img src="resources/ezk/images/slider/03.jpg" alt="Image 03" title="High Breed Male Goat " />
                    <img src="resources/ezk/images/slider/04.jpg" alt="Image 04" title="Best Dairy Cows Upto 35Litres per day " />
                    <img src="resources/ezk/images/slider/05.jpg" alt="Image 05" title="Best goats for meat grazing in the fields" />
                    <img src="resources/ezk/images/slider/06.jpg" alt="Image 06" title="Merino Sheep grazing" />
                    <img src="resources/ezk/images/slider/07.jpg" alt="Image 07" title="Field planted with cabbages within the county" />
                    <img src="resources/ezk/images/slider/08.jpg" alt="Image 08" title="Maize plantation with hybrid seeds" />
                    <img src="resources/ezk/images/slider/09.jpg" alt="Image 09" title="Wheat Harvesting the best machinery in town" />
                    <img src="resources/ezk/images/slider/10.jpg" alt="Image 10" title="Officers measuring milk at cooling station" />
                    <img src="resources/ezk/images/slider/12.jpg" alt="Image 12" title="Horticulture offering the best within the county" />
                    <img src="resources/ezk/images/slider/13.jpg" alt="Image 13" title="Poultry farming modernized for high productivity" />

                </div>
                <div id="htmlcaption" class="nivo-html-caption">
                    <strong>This</strong> is an example of a HTML caption with <a href="#">a link</a>.
                </div>

            </div>
            <script type="text/javascript" src="resources/ezk/js/jquery-1.4.3.min.js"></script>
            <script type="text/javascript" src="resources/ezk/js/jquery.nivo.slider.js"></script>
            <script type="text/javascript">
                $(window).load(function() {
                    $('#slider').nivoSlider();
                });
            </script>

            <div id="mid_left">
                <div id="mid_title">
                    Keeping Uasin Gishu County Informed

                </div>

                <p id="mid_text">  Information is a vital resource that every farmer needs for their success and high productivity, I-farm app. provides you all that </p>
                <div id="learn_more"><a href="#">Learn More</a></div>
            </div>
            <div class="cleaner"></div>

        </div>
    </div>
</div> <!-- end of header -->

<div id="tooplate_main"><h3><a href="#"></a></h3>
    <div class="col_3">
        <div class="title_with_icon"> <a href="#"><img src="resources/ezk/images/bandiat.jpeg" alt="image" /><h3>Current County Maize and wheat prices</h3></a></div>
        <div class="cleaner h10"></div>
        <p> I-farm Theme is targeting All manner of farmers</p>
    </div>
    <div class="col_3">
        <div class="title_with_icon"><a href="#"><img src="resources/ezk/images/toto.jpg" alt="image" /><h3>Current County Farm Inputs prices </h3></a></div>
        <div class="cleaner h10"></div>
        <p>The best prices that can be offered by various millers in town read more............ </p>
    </div>
    <div class="col_3 col_l">
        <div class="title_with_icon"><a href="#"><img src="resources/ezk/images/presentation.jpeg" alt="image" /><h3>Best County Dairy Farming</h3></a></div>
        <div class="cleaner h10"></div>
        <p> Dairy farming is taking the center role in income generating activity to many farmers of today since milk demand is on the rise always </p>
    </div>
    <div class="cleaner h30"></div>
    <div class="col_3">
        <div class="title_with_icon"><a href="#"><img src="resources/ezk/images/kukut.jpeg" alt="image" /><h3>Best County Poultry Farming</h3></a></div>
        <div class="cleaner h10"></div>
        <p>Layers and broilers farming becoming a major source of income to many farmers in the county know how I-farm can be so educative </p>
    </div>
    <div class="col_3">
        <div class="title_with_icon"><a href="#"><img src="resources/ezk/images/tundiat.jpeg" alt="image" /><h3>Horticulture Farming </h3></a></div>
        <div class="cleaner h10"></div>
        <p>Get the best tips on how many farmers are earning through horticulture</p>
    </div>
    <div class="col_3 col_l">
        <div class="title_with_icon"><a href="#"><img src="resources/ezk/images/vert.jpg" alt="image" /><h3>Farming Services</h3></a></div>
        <div class="cleaner h10"></div>
        <p>Various farmers are getting services and educative sessions that adds value to their input, with I-farm you can improve your yield, improve your breed and increase profitability in any form of farming.</p>
    </div>
    <div class="cleaner"></div>
    <div class="title_with_icon"><a href="www.google.com"><p><img src="resources/ezk/images/weather.jpg" alt="image" /></p><h3>Weather Forecast</h3></a></div>

</div>
<div id="tooplate_footer_wrapper">
    <div id="tooplate_footer">

        <div class="col_4">

        </div>

        <div id="tooplate_cr_bar_wrapper">
            <div id="tooplate_cr_bar">
                <label style="color:white">Copyright © 2015 <a href="#"></a> I-farm</label>
            </div>
        </div>
     </div>
</div>

</body>
</html>